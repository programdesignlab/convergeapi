var env = process.env.NODE_ENV || 'developement',
fs = require('fs'),
configPath = './config.json'
mongoskin = require('mongoskin');

var config = JSON.parse(fs.readFileSync(configPath, 'UTF-8'))[env];


exports.getDbInstance = function(){
    var db = mongoskin.db(this.getConnStr());
    return db;
}

exports.getConnStr = function(){
    var connStr = 'mongodb://' + config.database.host + ':' + config.database.port + '/' + config.database.db;
    //mongodb://<dbuser>:<dbpassword>@ds161029.mlab.com:61029/convergedb
    return connStr;
}

exports.getSecretKey = function(){
    return config.secret;
}

