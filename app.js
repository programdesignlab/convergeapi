var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var morgan = require('morgan');
const configManager = require('./lib/configManager');

var routes = require('./routes');
var app = express();

mongoose.connect(configManager.getConnStr());


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.set('superSecret', configManager.getSecretKey());

// use morgan to log requests to the console
app.use(morgan('dev'));

app.use(routes);

module.exports = app;