var Profile = require('../../models/profile'),
express = require('express'),
router = express.Router(),
jwt = require('jsonwebtoken'),
path = require('path'),
configManager = require(path.join(__dirname,'..', '..', 'lib', 'configManager'));

// profiles middleware
router.use('/profiles', (req, res, next) => {
    console.log('profile middeware');
    next();
});
 

//getting all profiles
//router.route('/profiles').get(function(req, res, next){       //can be used but cleaner with => operator
router.get('/profiles', (req, res, next) => {
    Profile.find(function(err, profiles){
        if(err){
            return res.send(err);
        }
        res.json(profiles);
    });
});
 

//creating a new profile
router.post('/profiles', (req, res, next) => {
    var profile = new Profile(req.body);

    profile.save(function(err){
        if(err){
            return res.send(err);
        }
        return res.json({message : 'Profile Added'});
    });
});
 
module.exports = router;