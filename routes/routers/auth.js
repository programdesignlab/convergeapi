var Profile = require('../../models/profile'),
express = require('express'),
router = express.Router(),
jwt = require('jsonwebtoken'),
path = require('path'),
configManager = require(path.join(__dirname,'..', '..', 'lib', 'configManager'));


// getting token
router.post('/login', (req, res, next) => {
    Profile.findOne({"email" : req.body.email }, function(err, profile){
        if(err){
            throw err;
        }
        if(!profile){
            res.json({ success: false, message: 'incorrect email or password' });//never tell user:Authentication failed. User not found.
        }
        else if(profile){
            if (profile.password != req.body.password) {
                res.json({ success: false, message: 'incorrect email or password' });
            } 
            else 
            {
                var token = jwt.sign(profile, configManager.getSecretKey(), { // TODO: will not store full profile in token
                    expiresIn: 1440 // expires in 24 hours
                }); 
                res.json({
                    success: true,
                    token: token
                });  
            }
        }
    }); 
});

module.exports = router;