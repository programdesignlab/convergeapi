var express = require('express'),
router = express.Router(),
mongoskin = require('mongoskin'),
path = require('path');

const configManager = require(path.join(__dirname,'..', '..', 'lib', 'configManager'))

router.use('/datasource/:datasourceName', (req, res, next) => {
    var db = configManager.getDbInstance();
    req.collection = db.collection(req.params.datasourceName)
    // console.log('/datasource middleware '+  req.params.datasourceName);
    next();
});  

router.get('/datasource/:datasourceName', (req, res, next) => {
    req.collection.find({} ,{limit: 10, sort: {'_id': -1}}).toArray(function(e, results){
    if (e) return next(e)
    res.send(results)
  })
});

module.exports = router; 


