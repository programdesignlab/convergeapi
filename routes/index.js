const router = require('express').Router(),
profile = require('./routers/profile'),
auth = require('./routers/auth'),
datasource = require('./routers/datasource'),
jwt = require('jsonwebtoken'),
path = require('path'),
configManager = require(path.join(__dirname,'..','lib', 'configManager'));

// default routes
router.get('/', (req, res) => {
    res.status(200).json({ message: 'Connected!'});
    
});


// routes middleware for all /api 
router.use('/api', function(req, res, next) {
  next();
  // var token = req.body.token || req.query.token || req.headers['x-access-token'];
  // if (token) {
  //   jwt.verify(token, configManager.getSecretKey(), function(err, decoded) {      
  //     if (err) {
  //       return res.json({ success: false, message: 'Failed to authenticate token.' });    
  //     } else {
  //       req.decoded = decoded;    
  //       next();
  //     }
  //   });

  // } else {
  //   return res.status(403).send({ 
  //       success: false, 
  //       message: 'No token provided.' 
  //   }); 
  // }
});


router.use('/', auth);
router.use('/api', profile);
router.use('/api', datasource);


module.exports = router; 