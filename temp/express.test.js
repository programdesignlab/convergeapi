var superagent = require('superagent')
var expect = require('expect')

describe('express rest api server', function(){
  var id

  it('posts an object', function(done){
    superagent.post('http://127.0.0.1:3000/collections/test')
      .send({ name: 'arvind'
        , email: 'arvind@converge.com',
        address: 'chennai'
      })
      .end(function(e, res){
        //console.log(res.body)
        expect(e).toEqual(null)
        expect(res.body.ops.length).toBeGreaterThan(0)
        expect(res.body.ops[0]._id.length).toEqual(24)
        id = res.body.ops[0]._id
        done()
      })
  })

  it('retrieves an object', function(done){
    superagent.get('http://127.0.0.1:3000/collections/test/'+id)
      .end(function(e, res){
        //console.log(res.body)
        expect(e).toEqual(null)
        expect(typeof res.body).toEqual('object')
        expect(res.body._id.length).toEqual(24)
        expect(res.body._id).toEqual(id)
        expect(res.body.name).toEqual('arvind')
        done()
      })
  })

  it('retrieves a collection', function(done){
    superagent.get('http://127.0.0.1:3000/collections/test')
      .end(function(e, res){
        //console.log(res.body)
        expect(e).toEqual(null)
        expect(res.body.length).toBeGreaterThan(0)
        //expect(res.body.map(function (item){return item._id})).toIncludeKey(id)
        done()
      })
  })

  it('updates an object', function(done){
    superagent.put('http://127.0.0.1:3000/collections/test/'+id)
      .send({ name: 'arvind kumar'
        , email: 'arvind@converge.com',
        address: 'chennai'
      })
      .end(function(e, res){
         console.log()
         expect(e).toEqual(null)
         expect(typeof res.body).toEqual('object')
         expect(res.body.msg.ok).toEqual(1) // 1 : ok 
        done()
      })
  })

  it('checks an updated object', function(done){
    superagent.get('http://127.0.0.1:3000/collections/test/'+id)
      .end(function(e, res){
        // console.log(res.body)
        expect(e).toEqual(null)
        expect(typeof res.body).toEqual('object')
        expect(res.body._id.length).toEqual(24)
        expect(res.body._id).toEqual(id)
        expect(res.body.name).toEqual('arvind kumar')
        done()
      })
  })

  it('removes an object', function(done){
    superagent.del('http://127.0.0.1:3000/collections/test/'+id)
      .end(function(e, res){
        //console.log(res.body)
        expect(e).toEqual(null)
        expect(typeof res.body).toEqual('object')
        expect(res.body.msg).toEqual('success')
        done()
      })
  })
  it('checks an removed object', function(done){
    superagent.get('http://127.0.0.1:3000/collections/test/')
      .end(function(e, res){
        // console.log(res.body)
        expect(e).toEqual(null)
        expect(res.body.map(function (item){return item._id})).toNotBe(id)
        done() 
      })
  })
 })
