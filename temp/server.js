var express = require('express');
var app = express();


var mongoose = require('mongoose');

var kuttiSchema = mongoose.Schema({
    name : String
});

var kutti = mongoose.model('kutti',kuttiSchema);


var destiny = new kutti({name : 'destiny'});
console.log(destiny.name); 

mongoose.connect('mongodb://127.0.0.1:27017/test');


var db = mongoose.connection;
db.on('error',console.error.bind(console,'connection error'));


db.once('connected', function(){
    console.log('connected');
});


destiny.save(function (err, destiny){
    if(err) return console.error(err);
})

kutti.find(function(err, kutties){
     if(err) return console.error(err);
     console.log(kutties);
})


app.get('/', function(req, res){  
    res.json({message : 'hooray! welcome to our api.'});
});

app.listen(process.env.port || 7000);
console.log('server running on 7000');