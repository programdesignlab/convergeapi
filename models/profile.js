var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userRoles = 'guest viewer analyst admin superadmin'.split(' ');

var profileSchema = new Schema({
    firstName : { required: true, type: String, trim: true },
    lastName : { type: String, trim: true },
    email : { required: true, type: String, trim: true },
    password :{ required: true, type: String },
    role : { type: String, enum: userRoles, required: true, default: userRoles[0] }    
});

module.exports = mongoose.model('Profile', profileSchema);